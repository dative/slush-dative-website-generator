'use strict';

var gulp = require('gulp'),
    rename = require('gulp-rename'),
    sequence = require('run-sequence');

gulp.task('clean', function () {
  var del = require('del');
  return del('./templates');
});

gulp.task('copyDotFiles', function () {
  
  gulp.src([
        'src/**/.*',
        '!src/.git',
        '!src/node_modules/**/.*',
        '!src/bower_components/**/.*',
      ])
      .pipe(rename(function (file) {
          if (file.basename[0] === '.') {
              file.basename = '-' + file.basename.slice(1);
          }
      }))
      .pipe(gulp.dest('./templates'));
});

gulp.task('copySrcFiles', function () {
  
  gulp.src([
        'src/*',
        '!src/node_modules',
        '!src/bower_components',
        '!src/gulp-tasks',
        '!src/src',
        'src/{src,gulp-tasks}/**',
      ])
      .pipe(rename(function (file) {
          if (file.basename[0] === '.') {
              file.basename = '-' + file.basename.slice(1);
          }
      }))
      .pipe(gulp.dest('./templates'));
});

gulp.task('compile', function () {

  sequence('clean', 'copyDotFiles', 'copySrcFiles');

});
