/*
 * slush-slush-dative-website-generator
 * https://github.com/webrgp/slush-slush-dative-website-generator
 *
 * Copyright (c) 2017, Rodrigo Passos
 * Licensed under the MIT license.
 */

'use strict';

var gulp = require('gulp'),
    fs = require('fs'),
    install = require('gulp-install'),
    conflict = require('gulp-conflict'),
    template = require('gulp-template'),
    rename = require('gulp-rename'),
    _ = require('underscore.string'),
    inquirer = require('inquirer'),
    path = require('path');

function format(string) {
    var username = string.toLowerCase();
    return username.replace(/\s/g, '');
}

var defaults = (function () {
    var homeDir, osUserName, configFile, user;

    if (process.platform === 'win32') {
        homeDir = process.env.USERPROFILE;
        osUserName = process.env.USERNAME || path.basename(homeDir).toLowerCase();
    }
    else {
        homeDir = process.env.HOME || process.env.HOMEPATH;
        osUserName = homeDir && homeDir.split('/').pop() || 'root';
    }

    configFile = path.join(homeDir, '.gitconfig');
    user = {};

    if (require('fs').existsSync(configFile)) {
        user = require('iniparser').parseSync(configFile).user;
        user.email = process.env.GIT_COMMITTER_EMAIL || user.email;
    }

    return {
        appName: 'Awesome Project',
        authorName: user.name || '',
        authorEmail: user.email || ''
    };
})();

gulp.task('default', function (done) {
    var prompts = [{
        type: 'input',
        name: 'appName',
        message: 'Wanna give this project a name?',
        default: defaults.appName
    },{
        type: 'input',
        name: 'appNameSlug',
        message: 'Where are you build this AWESOME project?',
        default: _.slugify(defaults.appName),
        filter: function( input ) {
          return _.slugify(input);
        },
        validate: function( input ) {
          return fs.existsSync(path.join(process.cwd(), _.slugify(input))) ? "This directory name is already taken" : true;
        }
    },{
        type: 'input',
        name: 'clientName',
        message: 'Who is this website for?',
    }, {
        type: 'input',
        name: 'appDescription',
        message: 'What is the description?'
    }, {
        type: 'input',
        name: 'authorName',
        message: 'What is the author name?',
        default: defaults.authorName
    }, {
        type: 'input',
        name: 'authorEmail',
        message: 'What is the author email?',
        default: defaults.authorEmail
    }, {
        type: 'confirm',
        name: 'moveon',
        message: 'Continue?'
    }];

    inquirer.prompt(prompts)
            .then(function (answers) {
                if (!answers.moveon) {
                    return done();
                }
                answers.appNameSlug = _.slugify(answers.appNameSlug);
                gulp.src(__dirname + '/templates/**')
                    .pipe(template(answers))
                    .pipe(rename(function (file) {
                        if (file.basename[0] === '-') {
                            file.basename = '.' + file.basename.slice(1);
                        }
                    }))
                    .pipe(conflict('./'))
                    .pipe(gulp.dest( path.join(process.cwd(), answers.appNameSlug) ))
                    .pipe(install({ignoreScripts: true}))
                    .on('end', function () {
                        done();
                    });
            });
});
